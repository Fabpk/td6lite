package uvsq;

public class Fraction
{
    private float denominateur;
    private float numerateur;

    static Fraction ZERO() {return new Fraction(0, 1);}
    static Fraction UN() {return new Fraction(1, 1);}


    public Fraction(float denom, float nume)
    {
        denominateur = denom;
        numerateur = nume;
    }

    public Fraction()
    {
        denominateur=1.0f;
        numerateur=0.0f;
    }

    float getNum(){return numerateur;}

    float getDen(){return denominateur;}
}
